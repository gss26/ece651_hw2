ECE651 - Software Engineering
HW1 - First Java Program

Gayan Seneviratna
gss26

Thanks for grading my project! This program functions as a reader for a 
"database" of information about Duke students and professors. 
(The data is stored as .csv files, to ensure smaller file sizes and 
avoid any issues with trying to setup a JDBC connection).<

a) Made it significantly easier to debug.

b) Leaves available the option to extend the program to for more complicated
data update and storage. 

Because I build my project as a maven project, my steps to run are 
slightly different than the directions specify. You'll of course
need to have maven set up and installed on your path. 

To build my project, use 

mvn project

This might take a while on the first building, since it needs to import 
and put together a few non-standard packages from the maven repository.
To run the program, use 

java -jar target/*SNAPSHOT*dependencies.jar main.main

This should bring up the menu and options. From there, you can follow the
instructions to run the program. Enjoy!