/* Catalog.java
 * 
 * A wrapper for a map of departments and courses in them. Used as a reference for 
 * holding information about students and professors.
 */


package academics;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.Iterator;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;


import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;	


public class Catalog {

	//Holds course and department information
	Map<Department, Vector<Course>> allCourses;
	
	//Defulat constructor
	public Catalog() {
		super();
		this.allCourses = new HashMap<Department, Vector<Course>>();
	}

	//Adds a department with no classes attached.
	public void addDepartment(Department department) {
		allCourses.put(department, new Vector<Course>());
	}
	
	//Adds a department, using its string constructor.
	public void addDepartment(String departmentName) {
		allCourses.put(new Department(departmentName),
					   new Vector<Course>());
	}
	
	//Adds a course to its department. 
	public void addCourse(Course course) {
		
		//Look up the department via the id.
		allCourses.keySet().forEach(key->{
		if(key.getDepartmentId().equalsIgnoreCase(course.getDepartmentId())) {
				allCourses.get(key).add(course);
			}
		});
	}
	
	//Allows looking up a department via it's department id. 
	public Department getDepartment(String departmentId) {
		for (Department key : allCourses.keySet()) {
			if(key.getDepartmentId().equalsIgnoreCase(departmentId)) {
				return key;
			}
		}
		return null;
	}
	
	//Looks up the set of courses for a given department.
	public Vector<Course> getCourses(Department department){
		return allCourses.get(department);
	}
	
	//Looks up the set of courses using the department id.
	public Vector<Course> getCourses(String departmentId) {
		
		//Doing an ID lookup requires iterating through the keys.
		for (Department key : allCourses.keySet()) {
			if(key.getDepartmentId().equalsIgnoreCase(departmentId)) {
				return allCourses.get(key);
			}
		}
		return null;
	}
	
	//Looks up the set of courses taught by a given professor.
	public Vector<Course> getCourses(String departmentId, int professorID){
		
		//Identify all the courses in the relevant department	
		Vector<Course> departmentCourses = getCourses(departmentId);
		
		//For each relevant one, identify whether it is taught by the specified professor
		Vector<Course> professorsCourses = new Vector<Course>();			
		for(Course course : departmentCourses) {
			if (course.getProfessorSSN() == professorID) {
				professorsCourses.add(course);
			}
		}
		
		return professorsCourses;
	}
	
	
	//Get the course specified by a given id
	public Course getCourse(Department department, int courseID) {
		
		//Identify all the courses in the relevant department	
		Vector<Course> departmentClasses = allCourses.get(department);
		
		//For each relevant one, identify whether it is taught by the specified professor
		for(int i = 0; i < departmentClasses.size(); i++) {
			if(departmentClasses.get(i).getCourseId() == courseID) {
				return departmentClasses.get(i);
			}
		}
		return null;
	}
	
	//Get the course specified by a given id. Uses the department Id rather than a a department object.
	public Course getCourse(String departmentId, int courseId) {
		
		//Has to go through all the department do the the indexing
		for (Department key : allCourses.keySet()) {
			
			//Upon finding a match, search for the matching course id
			if(key.getDepartmentId().equals(departmentId)) {
				for(Course course : allCourses.get(key)) {
					if(course.getCourseId() == courseId) {
						return course;
					}		
				}
			}
		}
		
		//Return null on failure
		return null;
	}
	
	//Return the number of courses overall.
	public int size() {
		return allCourses.size();		
	}
	
	//Return the number of courses listed under a given department.
	public int size(Department department) {
		return allCourses.get(department).size();
	}
	
	//Return the number of courses listed under a given department.
	public int size(String departmentId) {
		
		//Looks up with keyId, so have to go through all keys
		for (Department key : allCourses.keySet()) {
			if(key.getDepartmentId() == departmentId) {
				return allCourses.get(key).size();
			}
		}
		return 0;
	}
	
	//Reads department information from a csv file.
	public void readDepartments(String filePath) throws IOException {	
		
		//Open the reader
		try (
	            Reader reader = Files.newBufferedReader(Paths.get(filePath));
	        ){
			   
			//Build a reader specified to the department class annotations
			CsvToBean<Department> converter = new CsvToBeanBuilder<Department>(reader)
                    .withType(Department.class)	
                    .build();
			
			//Go through the file until end, adding empty course lists for departments
			Iterator<Department> departmentIterator = converter.iterator();
			while(departmentIterator.hasNext()) {
				allCourses.put(departmentIterator.next(), new Vector<Course>());
			}		
		}
	}
	
	//Reads course information from a csv file.	
	public void readCourses(String filePath) throws IOException {	
		
		//Open the reader
		try (
	            Reader reader = Files.newBufferedReader(Paths.get(filePath));
	        ){

			//Build a reader specified to the department class annotations
		    CsvToBean<Course> converter = new CsvToBeanBuilder<Course>(reader)
                    .withType(Course.class)	
                    .build();
			
		  //Go through the file until end, filling the appropriate department's vector
   		  Iterator<Course> courseIterator = converter.iterator();
		  while(courseIterator.hasNext()) {
			  Course newCourse = courseIterator.next();
				addCourse(newCourse);
				
		  }		
		}
	}

	
}
