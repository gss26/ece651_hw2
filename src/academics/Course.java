/* Course.java
 * 
 * An object to store information about a single course at Duke
 */

package academics;
import com.opencsv.bean.CsvBindByName;

public class Course {

	//CSV Bindings for the bean reader.
	
	@CsvBindByName
	String departmentId;
	
	@CsvBindByName
	String name;

	@CsvBindByName
	int courseId;
	
	//Includes the professor id who is teaching the course.
	@CsvBindByName
	int professorSSN;
	
	//Default constructor needed for the bean converter
	public Course() {
		super();
	}
	
	//Field constructor
	public Course(String departmentId, String name, int courseId, int professorSSN) {
		super();
		this.departmentId = departmentId;
		this.name = name;
		this.courseId = courseId;
		this.professorSSN = professorSSN;
	}
	

	//Field constructor that accepts a department object instead
	public Course(Department department, String name, int courseId, int professorSSN) {
		super();
		this.departmentId = department.getDepartmentId();
		this.name = name;
		this.courseId = courseId;
		this.professorSSN = professorSSN;
	}
	
	//Getters

	public String getDepartmentId() {
		return departmentId;
	}

	public String getName() {
		return name;
	}

	public int getCourseId() {
		return courseId;
	}

	
	public int getProfessorSSN() {
		return professorSSN;
	}

	//Setters
	
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public void setProfessorSSN(int professorSSN) {
		this.professorSSN = professorSSN;
	}
	
	//Courses are identified as equal if they hold the same department and id
	public boolean equals(Object other) {
		Course otherCourse = (Course) other;
	
		return ((this.courseId == otherCourse.getCourseId()) &&
				(this.departmentId == otherCourse.getDepartmentId()));
		
	}

	//Convert the course to a string for printing
	public String toString() {
		return departmentId + " " + courseId + ": " + name;
	}
	
}
