/* Department.java
 * 
 * An object to store information about a department at duke, which classe, students, and professors
 * Will include as a member variable.
 */

package academics;
import java.lang.Math;	
import com.opencsv.bean.CsvBindByName;

public class Department {

	@CsvBindByName
	String name;
	
	@CsvBindByName
	String departmentId;
	
	//Default constructor
	public Department() {
		this.name = "";
		this.departmentId = "";
	}
	
	//Field constructor
	public Department(String name, String departmentId) {
		super();
		this.name = name;
		this.departmentId = departmentId;
	}

	//Field constructor for no id. Will generate an acronym from the name.
	public Department(String name) {
		super();
		this.name = name;
		this.departmentId = name.substring(0, Math.min(name.length() - 1, 3)).toUpperCase();
	}
	
	//Getters 
	
	public String getName() {
		return name;
	}

	public String getDepartmentId() {
		return departmentId;
	}
	
	//Departments are considered equal if they share the same department id
	public boolean equals(Object other) {
		return this.departmentId.equalsIgnoreCase(((Department) other).getDepartmentId());	
	}
	
	//Hashing needed for the use of department as a key. Based entirely on the id.
	public int hashCode() {	
		return this.departmentId.hashCode();
	}
	
	
	//Converts department to string for printing.
	public String toString() {	

		//Handles default Department objects
		if (name.contentEquals("")) {
			return "an unspecified department";
		} else {
			return "the " + name +  " (" + departmentId + ") department"; 
		}
		
		
	}
	
}
