/* Address.java
 * 
 * An object to store basics of a North Carolina address
 */
 

package contactInfo;

 import com.opencsv.bean.CsvBindByName;

public class Address {
	
	String street1;
	String street2;
	String city;
	String zipCode;
	
	//Default constructor
	public Address() {
		super();
		this.street1 = "305 Teer Engineering Building";
		this.street2 = "Box 90271";
		this.city = "Durham";
		this.zipCode = "27705";
	}
	
	//Field constructor	
	public Address(String street1, String street2, String city, String  zipCode) {
		super();
		this.street1 = street1;
		this.street2 = street2;
		this.city = city;
		this.zipCode = zipCode;
	}
	
	//Addresses are considered equal if all of their fields are equivalent
	public boolean equals(Object other) {
		Address otherAddress = (Address) other;	
		if (!street1.equals(otherAddress.street1)) return false;
		if (!street2.equals(otherAddress.street2)) return false;
		if (!city.equals(otherAddress.city)) return false;
		if (!zipCode.equals(otherAddress.zipCode)) return false;
		return true;
	}
	
	//Convert an address to a string for printing.
	public String toString() {
		if (street2.length() == 0) {
			return street1 + ", " + city + ", NC, USA, " + zipCode;
		} else {
			return street1 + ", " + street2 + ", " + city + ", NC, USA, " + zipCode;
		}
	}
	
}
