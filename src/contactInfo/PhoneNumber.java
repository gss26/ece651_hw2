/* PhoneNumber.java
 * 
 * An object to store a phone number info
 */

package contactInfo;

public class PhoneNumber {	

	//A classification of the phone
	public enum PhoneType {
		MOBILE, HOME, WORK, OTHER;
	}	

	String areaCode;	
	String prefix;
	String lineNumber;
	PhoneType type;
	
	//Field constructor
	public PhoneNumber(String areaCode, String prefix, String lineNumber, PhoneType type) {
		super();
		this.areaCode = areaCode;
		this.prefix = prefix;
		this.lineNumber = lineNumber;
		this.type = type;
	}
	
	//Field constructor, converts the PhoneType to the enum
	public PhoneNumber(String areaCode, String prefix, String lineNumber, String type) {
		super();
		this.areaCode = areaCode;
		this.prefix = prefix;
		this.lineNumber = lineNumber;
		this.type = PhoneType.valueOf(type.toUpperCase());
	}
	
	//Field constructor assumes a default phone type
	public PhoneNumber(String areaCode, String prefix, String lineNumber) {
		super();
		this.areaCode = areaCode;
		this.prefix = prefix;
		this.lineNumber = lineNumber;
		this.type = PhoneType.OTHER;
	}
	
	//Converts to a string for printing
	public String toString() {
		String output = "";		
		switch(type)
		{
			case MOBILE:
				output += "mobile phone, ";
			break;
			case HOME:
				output += "home phone, ";				
			break;
			case WORK:
				output += "work phone, ";
			break;
			default:
				output += "phone, "	;			
			break;
		}
		output += "(" + areaCode + ")-" + prefix + "-" + lineNumber;
		return output;
	}

}
