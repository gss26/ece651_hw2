/*
 * BDStudent.java
 * 
 * A Duke professor.
 */

package humans;

import java.util.Vector;

import academics.*;
import contactInfo.*;
import roles.Position;
import roles.WorkHistory;

public class BDProfessor extends BlueDevil {

	public enum Title{
		Adjuct, Assistant, Practice, Professor, Emeritus, None;
	}

	Title title;
	
	//Conversion  constructor
	public BDProfessor(Person p) {
		super(p.ssn, p.firstName, p.lastName, p.age, p.sex, p.address, p.number);
		title = Title.None;
	}
	
	//Field constructor that adds a default work history, gpa, and level
	public BDProfessor(int ssn, String firstName, String lastName, int age, Sex sex,
			Address address, PhoneNumber number) {
		super(ssn, firstName, lastName, age, sex, address, number);
		title = Title.None;
	}

	//Field constructor
	public BDProfessor(int ssn, String firstName, String lastName, int age, Sex sex,
			Address address, PhoneNumber number, WorkHistory workHistory,
			Department department,
			Vector<Course> courses,
			Title title) {
		super(ssn, firstName, lastName, age, sex, address, number, workHistory, department, courses);
		this.title = title;
	}
	
	//Getters and settesr
	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}
	
	//Printing function. Includes professor-specific information
	public void whoIs() {
	
		//Selects the pronouns to use in printing.
		String pronounSubject;
		String pronounPossessive; 
		String pronounVerbDo;
		switch(sex) {
			case MALE:
				pronounSubject = "He";
				pronounPossessive = "his";
				pronounVerbDo = "is";
			break;
			case FEMALE:
				pronounSubject = "She";
				pronounPossessive = "her";
				pronounVerbDo = "is";
				break;
			default:
				pronounSubject = "They";
				pronounPossessive = "their";
				pronounVerbDo = "are";
			break;
		}

		//Builds the output beginning with name and professor type.
		String output = "";
		output += "Dr." + firstName + " " + lastName + " is ";
		switch(title) {
			case Adjuct:
				output += "an adjunct professor in  ";
			break;
			case Assistant:
				output += "a assistant professor in ";
			break;
			case Practice:
				output += "a professor of the practice in ";
			break;
			case Emeritus:
				output += "an emeritus professor in ";
			break;
			default:
				output += "a professor in ";
			break;
		}		
		
		//Adds information about department and demographics
		output += department.toString() + ". ";		
		output += pronounSubject + " "  + pronounVerbDo + " " + age + " years old ";
		output += "and lives at " + address + ". "; 
		
		//Adds out the most recent work history and has a default message for none.
		if(workHistory.size() == 0) {
			output += pronounSubject + " did not work before coming to Duke. ";
		} else {
			output += pronounSubject + " used to be a " + workHistory.get(0) + ".";	
		}	
		if(workHistory.size() > 1) {
			output += " Before that, " + pronounSubject + " used to be a " + workHistory.get(1) + "."; 
		}
		
		//Adds the set of courses they are teaching. 
		if (courses.size() == 0){
			output += pronounSubject + " " + pronounVerbDo + "  on sabbatical";
		} else {
			output += " " + pronounSubject + " " + pronounVerbDo + " teaching ";
			for(int i = 0; i < courses.size(); i++) {
				
				if(i != 0) {
					
					if (courses.size() > 2) {
						output += ", ";
					} else {
						output += " ";
					}
				}	
				
				if((i == courses.size() - 1) && (courses.size() > 1)) {
					output += "and ";
				}
				output += courses.get(i).toString();	
			}
		}
		output += ".";
		
		System.out.println(output);
		
	}
}
