/*
 * BDRoster.java
 * 
 * The heavy-lifter class here. Connects information between a catalog of courses 
 * and a roster of classes. Allows input reading from CSV files.
 */

package humans;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import com.opencsv.CSVReader;

import academics.Catalog;
import academics.Course;
import academics.Department;
import contactInfo.Address;
import contactInfo.PhoneNumber;
import humans.BDProfessor.Title;
import humans.BDStudent.level;
import humans.Person.Sex;
import roles.Volunteering;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BDRoster {
	
	Map<Integer, BlueDevil> blueDevils;
	Catalog catalog;
	
	//Constructor with for a blank map of Blue Devils.
	//Requires an already existent set of courses and Departments define.
	public BDRoster(Catalog catalog) {
		super();
		blueDevils = new HashMap<Integer, BlueDevil>();
		this.catalog = catalog;
	}
	
	//Field
	//Requires an already existent set of courses and Departments define.
	public BDRoster(Map<Integer, BlueDevil> blueDevils, Catalog catalog) {
		this.blueDevils = blueDevils;
		this.catalog = catalog;
	}
	
	//Prints all individuals in the roster (usually for testing purposes)
	public void printAll() {
		for(Integer key : blueDevils.keySet()) {
			blueDevils.get(key).whoIs();
		}
	}

	//Print all the students in a simple table
	public void printStudents() {
		
		//Header row
		System.out.format("%10s", "SSN");
		System.out.print("\t");
		System.out.format("%15s", "First Name");
		System.out.print("\t");
		System.out.format("%15s", "Last Name");
		System.out.println();
		
		//Iterate through all the blue devils
		for(Integer key : blueDevils.keySet()) {
			
			//Only print if it's a student
			BlueDevil bd = blueDevils.get(key);
			if(bd.getClass() == BDStudent.class) {
				System.out.format("%10d", bd.getSsn());
				System.out.print("\t");
				System.out.format("%15s", bd.getFirstName());
				System.out.print("\t");
				System.out.format("%15s",  bd.getLastName());
				System.out.println();
			}
		}
		
		System.out.println();
	}
	
	//Print all the professor in a simple table
	public void printProfessors() {
		
		//Header row
		System.out.format("%10s", "SSN");
		System.out.print("\t");
		System.out.format("%15s", "First Name");
		System.out.print("\t");
		System.out.format("%15s", "Last Name");
		System.out.println();
		
		//Iterate through all the blue devils
		for(Integer key : blueDevils.keySet()) {
			
			//Only print if it's a student
			BlueDevil bd = blueDevils.get(key);
			if(bd.getClass() == BDProfessor.class) {
				System.out.format("%10d", bd.getSsn());
				System.out.print("\t");
				System.out.format("%15s", bd.getFirstName());
				System.out.print("\t");
				System.out.format("%15s",  bd.getLastName());
				System.out.println();
			}
		}
		System.out.println();
	}
	
	//Print a given individual based on SSN for fast access
	public void printBlueDevil(int ssn) {
		if(blueDevils.containsKey(ssn)) {
			blueDevils.get(ssn).whoIs();
		} else {
			System.out.println("No individual with that SSN identified.");
		}
	}
	
	//Print all individuals with the same name. Will print all matches
	public void printBlueDevil(String firstName, String lastName) {
		
		int count = 0;
		for(Integer key : blueDevils.keySet()) {
			if(blueDevils.get(key).getFirstName().equalsIgnoreCase(firstName)) {
				if(blueDevils.get(key).getLastName().equalsIgnoreCase(lastName)) {
					count++;
					System.out.print(count + ") ");
					blueDevils.get(key).whoIs();
				}
			}
		}
		
		//If none found, print a default message
		if(count == 0)
		System.out.println("No matching name found.");
		
	}
	
	//Print out all the information about a class. 
	public void printCourse(String departmentId, int courseId) {
		
		//Identify if the course exists; if not, print out an error message.
		Course currentCourse = catalog.getCourse(departmentId, courseId);
		if (currentCourse == null) {
			System.out.println("No course by those specifications found!");
		} else {
			
			//Get the course from the catalog
			System.out.println(catalog.getCourse(departmentId, courseId));
			
			//Identify the relevant professor against the roster and print their name
			BDProfessor professor = (BDProfessor) blueDevils.get(currentCourse.getProfessorSSN());			
			System.out.println("Professor: " + professor.getFirstName() + " " +  professor.getLastName());
			
			//Print out the students identified as being in that class.
			System.out.println("Students:");
			for(Integer key : blueDevils.keySet()) {
				if(blueDevils.get(key).getClass() == BDStudent.class)
				{
					BDStudent student = (BDStudent) blueDevils.get(key);
					
					if (student.getCourses().contains(currentCourse)) {
						System.out.println(student.getFirstName() + " " + student.getLastName());
					}
				}
			}
		}
	}
	
	//Read all basic demographic information for a person from a row in the data
	public Person parsePerson(String nextRow[]) {
	
		int ssn = Integer.parseInt(nextRow[0]);
		String firstName = nextRow[2];
		String lastName = nextRow[3];
		int age = Integer.parseInt(nextRow[4]);
		
		
		//Sex is expected as a single character 
		Sex sex;
		if (nextRow[5].equalsIgnoreCase("M")) {
			sex = Person.Sex.MALE;
		} else if (nextRow[5].equalsIgnoreCase("F")) {
			sex = Person.Sex.FEMALE;
		} else {
			sex = Person.Sex.OTHER;
		}
		
		Address address = new Address(nextRow[6], nextRow[7], nextRow[8], nextRow[9]);
		
		//Reads a number starting with a phone type
		PhoneNumber number;
		if(nextRow[10].equalsIgnoreCase("M")) {
			number = new PhoneNumber(nextRow[11], nextRow[12], nextRow[13], PhoneNumber.PhoneType.MOBILE);
		} else if(nextRow[9].equalsIgnoreCase("H")) {
			number = new PhoneNumber(nextRow[11], nextRow[12], nextRow[13], PhoneNumber.PhoneType.HOME);
		} else if(nextRow[9].equalsIgnoreCase("W")) {
			number = new PhoneNumber(nextRow[11], nextRow[12], nextRow[13], PhoneNumber.PhoneType.WORK);
		} else {
			number = new PhoneNumber(nextRow[11], nextRow[12], nextRow[13], PhoneNumber.PhoneType.OTHER);
		}
		
		return new Person(ssn, firstName, lastName, age, sex, address, number);
	}

	//Read all students from a file
	public void readStudents(String filePath) throws IOException {
		
		try(
	            CSVReader csvReader = new CSVReader(Files.newBufferedReader(Paths.get(filePath)));
			){
			
			//Reads one row at a time, skipping the header.
			String [] nextRow = csvReader.readNext();
			nextRow = csvReader.readNext();
			while(nextRow != null) {
					
				//Generates a student initially as a person only
				BDStudent newStudent = new BDStudent(parsePerson(nextRow));
				
				//Adds student related fields
				newStudent.setGpa(Double.parseDouble(nextRow[14]));
				switch(nextRow[15]) {
					case("Undergraduate"):
						newStudent.setLevel(level.Undergraduate);
					break;
					case("Masters"):
						newStudent.setLevel(level.Masters);
					break;
					case("PhD"):
						newStudent.setLevel(level.PhD);
					break;
					case("PostDoc"):
						newStudent.setLevel(level.PostDoc);
					break;
					default:
						newStudent.setLevel(level.none);
					break;
				}	
				String departmentID = nextRow[1];
				newStudent.setDepartment(catalog.getDepartment(departmentID));
				blueDevils.put(newStudent.getSsn(), newStudent);
				
				nextRow = csvReader.readNext();
			}			
		}
	}
	
	//Read all professors from a file
	public void readProfessors(String filePath) throws IOException {
		
		try(
	            CSVReader csvReader = new CSVReader(Files.newBufferedReader(Paths.get(filePath)));
			)
		{
			
			//Reads one row at a time, skipping the header.
			String [] nextRow = csvReader.readNext();
			nextRow = csvReader.readNext();			
			while(nextRow != null)
			{
				
				//Generates a student initially as a person only				
				BDProfessor newProfessor = new BDProfessor(parsePerson(nextRow));	
				
				//Adds professor related fields
				switch(nextRow[12])
				{
					case "Adjuct":
						newProfessor.setTitle(Title.Adjuct);
					break;
					case "Assistant":
						newProfessor.setTitle(Title.Assistant);
					break;
					case "Practice":
						newProfessor.setTitle(Title.Practice);
					break;
					case "Emeritus":
						newProfessor.setTitle(Title.Emeritus);
					break;
				}				
				String departmentID = nextRow[1];
				newProfessor.setDepartment(catalog.getDepartment(departmentID));	
				newProfessor.setCourses(catalog.getCourses(departmentID, newProfessor.getSsn()));
				
				
				//Adds them to the full set of Duke-affiliated individuals
				blueDevils.put(newProfessor.getSsn(), newProfessor);	
				nextRow = csvReader.readNext();
			}
		}		
	}
	
	
	//Read student enrollment information and fill students with it
	public void readStudentCourses(String filePath) throws IOException {
		try(
	            CSVReader csvReader = new CSVReader(Files.newBufferedReader(Paths.get(filePath)));
			){
			
			//Read a row at a time
			String [] nextRow = csvReader.readNext();
			nextRow = csvReader.readNext();
			while(nextRow != null) {
				
				//Identify the student taking the course
				int ssn = Integer.parseInt(nextRow[0]);

				//Read info about the course
				String departmentId = nextRow[1];
				int courseId = Integer.parseInt(nextRow[2]);
				Course newCourse = catalog.getCourse(departmentId, courseId);			
				
				//If the course is in the catalog and the student exists,
				//add the course to the student's schedule.
				if(newCourse != null) {
					if(blueDevils.containsKey(ssn)) {
						blueDevils.get(ssn).addCourse(newCourse);
					}
				}	
				nextRow = csvReader.readNext();			
			}		
		}
	}
	
	//Read individual work history
	public void readWorkHistory(String filePath) throws IOException {
		try(
	            CSVReader csvReader = new CSVReader(Files.newBufferedReader(Paths.get(filePath)));
			){
			
			//Read a row at at time
			String [] nextRow = csvReader.readNext();
			nextRow = csvReader.readNext();
			while(nextRow != null) {
				
				//Identify if the specified individual exists
				int ssn = Integer.parseInt(nextRow[0]);
				if(blueDevils.containsKey(ssn)) {

					//Read a volunteer position and add it to the work history
					if(nextRow[1].equals("V")) {
						
						String organization = nextRow[2];
						String title = nextRow[3];
						String issue = nextRow[4];
						int date = Integer.parseInt(nextRow[5]);
						blueDevils.get(ssn).addWorkHistory(new Volunteering(date, organization, title, issue));
					}
					
					//Read a job position and add it to the work history
					if(nextRow[1].equals("J")) {
						String organization = nextRow[2];
						String title = nextRow[3];
						String role = nextRow[4];
						int date = Integer.parseInt(nextRow[5]);
						blueDevils.get(ssn).addWorkHistory(new Volunteering(date, organization, title, role));
					}
				}
				
				nextRow = csvReader.readNext();			
				
			}		
		}
	}
	
}
