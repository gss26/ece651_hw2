/*
 * BDStudent.java
 * 
 * A Duke student.
 */

package humans;

import java.util.Vector;

import academics.Course;
import academics.Department;
import contactInfo.Address;
import contactInfo.PhoneNumber;
import humans.Person.Sex;
import roles.Position;
import roles.WorkHistory;


public class BDStudent extends BlueDevil{
	
	public enum level {
		Undergraduate, Masters, PhD, PostDoc, none;
	}
	
	double gpa;
	level level;
	
	//Conversion constructor
	public BDStudent(Person p) {
		super(p.ssn, p.firstName, p.lastName, p.age, p.sex, p.address, p.number);
		this.gpa = 0;
		this.level = level.none;
	}
	
	//Field constructor that adds a default work history, gpa, and level
	public BDStudent(int ssn, String firstName, String lastName, int age, Sex sex,
			Address address, PhoneNumber number) {
		super(ssn, firstName, lastName, age, sex, address, number);
		this.gpa = 0;
		this.level = level.none;
	}
	
	//Field constructor
	public BDStudent(int ssn, String firstName, String lastName, int age, Sex sex,
			Address address, PhoneNumber number, WorkHistory workHistory,
			Department department, Vector<Course> courses,
			double gpa, level level) {
		super(ssn, firstName, lastName, age, sex, address, number, workHistory, department, courses);
		this.gpa = gpa;
		this.level = level;

	}
	
	//Getters and setters

	
	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) {
		this.gpa = gpa;
	}

	public level getLevel() {
		return level;
	}

	public void setLevel(level level) {
		this.level = level;
	}

	//Printing function. Includes student-specific information
	public void whoIs() {
		
		//Selects the pronouns to use in printing.
		String pronounSubject;
		String pronounPossessive; 
		String pronounVerbDo;
		switch(sex) {
			case MALE:
				pronounSubject = "He";
				pronounPossessive = "his";
				pronounVerbDo = "is";
			break;
			case FEMALE:
				pronounSubject = "She";
				pronounPossessive = "her";
				pronounVerbDo = "is";
				break;
			default:
				pronounSubject = "They";
				pronounPossessive = "their";
				pronounVerbDo = "are";
			break;
		}

		//Builds the output beginning with name and student type.
		String output = "";
		output += firstName + " " + lastName + " is ";
		switch(level) {
			case Undergraduate:
				output += "an undergraduate student in ";
			break;
			case Masters:
				output += "a graduate student in ";
			break;
			case PhD:
				output += "a PhD candidate student in ";
			break;
			case PostDoc:
				output += "an postdoctal scholar in ";
			break;
			default:
				output += "a student in ";
			break;
		}
		
		//Adds information about department and demographics
		output += department.toString() + ". ";		
		output += pronounSubject + " " + pronounVerbDo + " " + age + " years old ";
		output += "and lives at " + address + ". "; 
		
		//Adds a message about GPA without revealing the actual gpa.
		if(gpa >= 3.5) {
			output += pronounSubject + " " + pronounVerbDo + " doing quite well in " + pronounPossessive + " studies. ";
		} else if (gpa > 3.0) {
			output += pronounSubject + " " + pronounVerbDo + " doing alright in " + pronounPossessive + " studies. ";
		} else {
			output += pronounSubject + " might need to spend more time studying. ";
		}
	
		//Adds out the most recent work history and has a default message for none.
		if(workHistory.size() == 0) {
			output += " " + pronounSubject + " does not have any prior work experience. ";
		} else {
			
			output += " " + pronounSubject + " used to be a " + workHistory.get(0) + ".";	
		}
		if(workHistory.size() > 1) {
			output +=" " + pronounSubject + " used to be a " + workHistory.get(1) + " before that ."; 
		}

		//Adds the set of courses they are taking. 
		if (courses.size() == 0){
			output += " " + pronounSubject + " " + pronounVerbDo + " not taking any classes right now";
		} else {

			output += " " + pronounSubject + " " + pronounVerbDo + " taking ";
			
			for(int i = 0; i < courses.size(); i++) {
				
				if(i != 0) {
					
					if (courses.size() > 2) {
						output += ", ";
					} else {
						output += " ";
					}
				}	
				
				if((i == courses.size() - 1) && (courses.size() > 1)) {
					output += "and ";
				}

				output += courses.get(i).toString();	
			}
		}
		output += ".";
		
		System.out.println(output);
		
	}



}
