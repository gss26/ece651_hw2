/*
 * BlueDevil.java
 * 
 * Defines an abstract Duke individual. Adds fields to the person that would be expected of an
 * affiliate and requires child classes to implement printing.
 */

package humans;
import academics.*;
import contactInfo.Address;
import contactInfo.PhoneNumber;
import roles.Position;
import roles.WorkHistory;

import java.util.Vector;

public abstract class BlueDevil extends Person {

	Department department;
	Vector<Course> courses;
	
	//Constructor that begins with empty work history and courses, as well as a default department.
	public BlueDevil(int ssn, String firstName, String lastName, int age, Sex sex,
			Address address, PhoneNumber number) {
		
		super(ssn, firstName, lastName, age, sex, address, number, new WorkHistory());
		this.department = new Department();
		this.courses = new Vector<Course>();
	}
	
	//Full field constructor
	public BlueDevil(int ssn, String firstName, String lastName, int age, Sex sex,
			Address address, PhoneNumber number, WorkHistory workHistory,
			Department department, Vector<Course> courses) {
		
		super(ssn, firstName, lastName, age, sex, address, number, workHistory);	
		
		this.department = department;
		this.courses = courses;
	}
	
	//Getters and setters 
	
	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
	
	public Vector<Course> getCourses() {
		return courses;
	}

	public void setCourses(Vector<Course> courses) {
		this.courses = courses;
	}

	//Adds a class to an individual's current schedule	
	public void addCourse(Course course) {
		courses.add(course);
	}
	
	//Print information about an individual
	abstract void whoIs();

}
