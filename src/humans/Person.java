/*
 * Person.java
 * 
 * Stores basic information relevant to a person.
 */


package humans;
import contactInfo.*;
import roles.*;
import java.util.Vector;

public class Person {

	public enum Sex {
		MALE, FEMALE, OTHER;
	}
	
	int ssn;
	String firstName;
	String lastName;
	int age;
	Sex sex;
	Address address;
	PhoneNumber number;
	WorkHistory workHistory;
	
	//Default constructor
	public Person() {}
	
	//Field constructor which creates an empty workhistory.
	public Person(int ssn, String firstName, String lastName, int age, Sex sex,
			Address address, PhoneNumber number) {
		super();
		this.ssn = ssn;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.sex = sex;
		this.address = address;
		this.number = number;
		this.workHistory = new WorkHistory();
	}	
	
	//Field constructor with a filled work history.
	public Person(int ssn, String firstName, String lastName, int age, Sex sex,
			Address address, PhoneNumber number,
			WorkHistory workHistory) {
		super();
		this.ssn = ssn;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.sex = sex;
		this.address = address;
		this.number = number;
		this.workHistory = workHistory;
	}

	//Getters and setters 
	
	public int getSsn() {
		return ssn;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public PhoneNumber getNumber() {
		return number;
	}

	public void setNumber(PhoneNumber number) {
		this.number = number;
	}

	public WorkHistory getWorkHistory() {
		return workHistory;
	}

	public void setWorkHistory(WorkHistory workHistory) {
		this.workHistory = workHistory;
	}	
	
	//Adds a position ot the object's work history
	public void addWorkHistory(Position position) {
		workHistory.addPosition(position);
	}
	

	
	
}
