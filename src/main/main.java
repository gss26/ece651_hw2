/*
 * Main.java
 * 
 * Handles IO with the BDRoster / Catalog backend.
 */

package main;

import java.io.IOException;

import academics.*;
import contactInfo.*;
import humans.*;
import roles.*;

import java.io.InputStream;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		
		//Defines and read the catalog form department and course files
		Catalog dukeCatalog = new Catalog();
		try {
			dukeCatalog.readDepartments("./data/departments.csv");
			dukeCatalog.readCourses("./data/courses.csv");
		} catch (IOException e) {
			System.err.println("Input data for coursework not found!");
		}
		
		//Defines and reads the roster from student, professor, student course history,
		// and work history files
		BDRoster dukeRoster = new BDRoster(dukeCatalog);
		try {
			dukeRoster.readStudents("./data/students.csv");
			dukeRoster.readProfessors("./data/professors.csv");
			dukeRoster.readStudentCourses("./data/enrollment.csv");
			dukeRoster.readWorkHistory("./data/history.csv");
		} catch (IOException e) {
			System.err.println("Input data for students and/or professors not found!");
		}
		
		//Welcome message
		System.out.println("Welcome to the Duke University Secret Backend System!");
		System.out.println("Available commands...");
		System.out.println("1: List all students.");
		System.out.println("2: List all professors.");
		System.out.println("3: Look up an individual by SSN.");
		System.out.println("4: Look up an indiviual by Name.");
		System.out.println("5: Look up a class.");
		System.out.println("Q: Quit the system.");
		System.out.println();
		System.out.println("Please enter a command: ");
        
		//Defines a scanner for an IO loop
		Scanner in = new Scanner(System.in);
        
		//Name the loop for exit purposes. Read values one at a tmie from the user
		inputloop:
        while(in.hasNext()){
        	String input = in.next();
 
            switch(input)
            {
            
            	//Prints a table of all students
            	case "1":
            		System.out.println();
            		dukeRoster.printStudents();
            	break;
            	
            	//Prints a table of all professors
            	case "2":
            		System.out.println();
            		dukeRoster.printProfessors();
            	break;
            	
            	//Call whoIs by SSN
            	case "3":
            		System.out.println();
            		System.out.println("Please give me the SSN of the individual you'd like to look up.");
            		System.out.println("If you don't have it, start by entering 'Q', then call 1 or 2 first.");
            		
            		input = in.next();
            	
            		if(!input.equalsIgnoreCase("Q")) {
            			try{
            				dukeRoster.printBlueDevil(Integer.parseInt(input));            			
            			}
            			catch(java.lang.NumberFormatException e) {
            				System.out.println("Parse failed. Please enter a number next time!");
            			}
            		}
            	break;
            	
            	//Call whoIs by name
            	case "4":
            		System.out.println("Please give me a first name.");
            		String firstName = in.next();
            		System.out.println("Please give me a last name.");
            		String lastName = in.next();
            		dukeRoster.printBlueDevil(firstName, lastName);
            	break;
            	
            	
            	//Call whoIs by name
            	case "5":
            		System.out.println("");
            		System.out.println("Please give me a department.");
            		String departmentId = in.next();
            		System.out.println("Please give me a class number.");
            		int classId = Integer.parseInt(in.next());
            		System.out.println();
            		dukeRoster.printCourse(departmentId, classId);
            	break;
            	
            	//Options to exit program
            	case  "Q":
            	break inputloop;
            	case  "q":
                break inputloop;
                
                //On undefined input, exit from the system.
            	default:
            		System.out.println();
            		System.out.println("Inavlid command! Valid commands are...");
                	System.out.println("1: List all students.");
            		System.out.println("2: List all professors.");
            		System.out.println("3: Look up an individual by SSN.");
            		System.out.println("4: Look up an indiviual by name.");
            		System.out.println("5: Look up a class.");
            		System.out.println("Q: Quit the system.");
            	break;	
            }

            System.out.print("\nPlease enter another command: ");
            
        }
        System.out.println("Thanks you for using the system. See you again soon.");
        
        in.close();
		
	}
}
