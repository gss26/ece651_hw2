/*
 * Job.java
 * 
 * Object to store information about a paid position
 */

package roles;

public class Job extends Position {
	
	String project;

	//Field constructor
	public Job(int date, String organization, String title, String project) {
		super(date, organization, title);
		this.project = project;
	}

	//Converts object to string for printing
	@Override
	public String toString() {
		return title + " at " + organization + " where he worked on " + project;		
	}

	//Jobs are equal to each other if all the string fields are equal
	@Override
	public boolean equals(Object o) {
		if(o.getClass() != Job.class) {
			return false;
		}
		
		return (organization.equals(((Job) o).organization)) &&
				(title.equals(((Job) o).title)) &&
				(project.equals(((Job) o).project));
	}

	@Override
	public int hashCode() {
		return organization.hashCode() +
				title.hashCode() + 
				project.hashCode();
				
	}
		
}
