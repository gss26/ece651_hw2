/*
 * Abstract class to define a role an individual had at some point in time
 */

package roles;

public abstract class Position {
	
	String organization;
	String title;
	int date;
	
	//Default constructor
	public Position() {super();}
	
	//Field constructor
	public Position(int date, String organization, String title) {
		super();
		this.organization = organization;
		this.title = title;
		this.date = date;
	}

	
	//Forces definitions of these functions to allow for printing / sorting
	abstract public String toString();
	abstract public boolean equals(Object o);
	abstract public int hashCode();
	
}
