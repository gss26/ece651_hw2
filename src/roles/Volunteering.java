/*
 * Job.java
 * 
 * Object to store information about a volunteer position
 */


package roles;

public class Volunteering extends Position {
	String issue;
	
	//Field constructor
	public Volunteering(int date, String organization, String title, String issue) {
		super(date, organization, title);
		this.issue = issue;
	}
	
	//Convert to string for printing
	@Override
	public String toString() {
		return title + " at " + organization + " helping to address " + issue;		
	}

	//Jobs are equal to each other if all the string fields are equal
	@Override
	public boolean equals(Object o) {
		if(o.getClass() != Volunteering.class) {
			return false;
		}
		
		return (organization.equals(((Volunteering) o).organization)) &&
				(title.equals(((Volunteering) o).title)) &&
				(issue.equals(((Volunteering) o).issue));
	}

	@Override
	public int hashCode() {
		return organization.hashCode() +
				title.hashCode() + 
				issue.hashCode();
				
	}
		
}
