/*
 * WorkHistory.java
 * 
 * Holds a set of information about work positions, sorted by date.
 */


package roles;
import java.util.*;

//Helper class to define how positions can be sorted in the work history by date
class SortByDate implements Comparator<Position> 
{ 

    public int compare(Position a, Position b) 
    { 
        return  b.date - a.date; 
    } 
} 


public class WorkHistory {

	List<Position> history;

	//Default constructor
	public WorkHistory() {
		super();
		this.history = new ArrayList<Position>();
	}
	
	//Field constructor
	public WorkHistory(List<Position> history) {
		super();
		this.history = history;
	}
	
	//Adds a position to the history, then sorts it
	public void addPosition(Position newPosition) {
		history.add(newPosition);
		sortPositions();
	}
	
	//Removes a given position from the history	
	public void removePositions(Position toRemove) {
		history.remove(toRemove);
	}
	
	//Calls the sort functionality using SortingByDate
	public void sortPositions() {
		Collections.sort(history, new SortByDate());
	}
	
	//Get the size of the current set
	public int size() {
		return history.size();
	}
	
	//Gets a given case. Will be in sorted order.
	public Position get(int i) {
		return history.get(i);
	}
	
	//Prints all pieces of work history.
	public void printPositions() {
		for (int i = 0; i < history.size(); i++) {		
			System.out.println(history.get(i));
		}
	}
}
