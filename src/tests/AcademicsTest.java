/* AcademicsTests.java
 * 
 * Same basic unit tests for the academics objects
 */

package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import academics.*;

class AcademicsTest {

	@Test
	void testDepartment() {	
		//Tests the printing and equality operations
		Department dep1 = new Department("Statistics");
		Department dep2 = new Department("Statistics", "STA");
		Department dep3 = new Department("Statistics", "STATS");
		System.out.println(dep1);
		System.out.println(dep2);
		System.out.println(dep3);
		assertEquals(dep1, dep2, "Comparison operator 1");
		assertEquals(dep1.getDepartmentId(), dep2.getDepartmentId(), "Abbreviation generation");
	}
	
	@Test
	void testCourse() {
		//Tests the printing and equality operations
		Department dep1 = new Department("Statistics");	
		Course course1 = new Course(dep1.getDepartmentId(), "Introduction to Statistics", 101, 111);
		Course course2 = new Course(dep1.getDepartmentId(), "Advanced Statistics", 301, 222);
		Course course3 = new Course(dep1.getDepartmentId(), "Bayesian Analysis", 200, 333);
		Course course4 = new Course(dep1.getDepartmentId(), "Introduction to Statistics", 101, 444);
		System.out.println(course1);
		System.out.println(course2);
		assertEquals(course1, course4);
	}
	
	@Test
	void testCatalog() {
		//Confirms the size of the catalog increases appropriately
		Department dep1 = new Department("Statistics");	
		Course course1 = new Course(dep1, "Introduction to Statistics", 101, 111);
		Course course2 = new Course(dep1, "Advanced Statistics", 301, 222);
		Catalog cat1 = new Catalog();
		cat1.addDepartment(dep1);
		cat1.addCourse(course1);
		cat1.addCourse(course2);
		assertEquals(cat1.size(), 1);
		assertEquals(cat1.size(dep1), 2);		
	}

	
	@Test
	void testCSVReader() {
		
		//Demonstrates the ability to read the csv 
		Catalog cat = new Catalog();
		try {
			cat.readDepartments("./data/departments.csv");
			assertEquals(cat.size(), 7);
			
			cat.readCourses("./data/courses.csv");
			assertEquals(cat.getCourses("STA").size(), 2);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
