/* ContactInfoTest.java
 * 
 * Same basic unit tests for the contact info objects
 */


package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import contactInfo.*;

class ContactInfoTest {

	@Test
	void testAddress() {
		
		Address address1 = new Address("123 Main Street", "", "Durham", "27705");
		Address address2 = new Address("123 Main Street", "Apt. 12", "Durham", "27705");
		Address address3 = new Address("123 Main Street", "", "Durham", "27705");
		
		System.out.println(address1);
		System.out.println(address2);
		System.out.println(address3);

		assertEquals(address1.toString(), "123 Main Street, Durham, NC, USA, 27705", "Succcess");
		assertEquals(address2.toString(), "123 Main Street, Apt. 12, Durham, NC, USA, 27705", "Success");	
		assertEquals(address1, address3);
	}
	
	@Test
	void testPhoneNumber() {
		
		PhoneNumber number1 = new PhoneNumber("555", "123", "4567");
		PhoneNumber number2 = new PhoneNumber("555", "123", "4568", "mobile");
	
		System.out.println(number1);
		System.out.println(number2);
		
		assertEquals(number1.toString(), "phone, (555)-123-4567");
		assertEquals(number2.toString(), "mobile phone, (555)-123-4568");
	}

}
