/* HumansTest.java
 * 
 * Same basic unit tests for the human objects
 */


package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.Vector;

import org.junit.jupiter.api.Test;
import academics.*;
import contactInfo.*;
import roles.*;
import humans.*;
import humans.BDStudent.level;
import humans.Person.Sex;

class HumansTest {

	@Test
	void testStudent() {
		
		Catalog catalog1 = new Catalog();
		catalog1.addCourse(new Course("STA", "Introduction to Bayesisan Theory", 601, 111));
		catalog1.addCourse(new Course("ENG", "Software Engineering", 651, 222));
		catalog1.addCourse(new Course("ENG", "Pattern Recognition", 681, 333));
		
		Vector<Course> courseLoad = new Vector<Course>();
		courseLoad.add(catalog1.getCourse("STA", 601));
		
		BDStudent student1 = new BDStudent(
				123456789,
				"Gayan",
				"Seneviratna",
				25,
				Person.Sex.MALE, 
				new Address("32 Argonaut Drive", "Mountain View", "Minnesota", "93933"),
				new PhoneNumber("555", "123", "4567", contactInfo.PhoneNumber.PhoneType.MOBILE));
		
		student1.whoIs();				
	}
	
	@Test
	void testProfessor() {
		
		Catalog catalog1 = new Catalog();
		catalog1.addCourse(new Course("STA", "Introduction to Bayesisan Theory", 601, 111));
		catalog1.addCourse(new Course("ENG", "Software Engineering", 651, 222));
		catalog1.addCourse(new Course("ENG", "Pattern Recognition", 681, 333));
		
		Vector<Course> courseLoad = new Vector<Course>();
		courseLoad.add(catalog1.getCourse("Statistics", 601));
		
		BDProfessor professor1 = new BDProfessor(
				123456788,
				"Gayan",
				"Seneviratna",
				25,
				Person.Sex.MALE, 
				new Address("32 Argonaut Drive", "Mountain View", "Minnesota", "93933"),
				new PhoneNumber("555", "123", "4567", contactInfo.PhoneNumber.PhoneType.MOBILE));
		
		professor1.whoIs();				
	}
	
	@Test
	void testReading() {
		
		Catalog catalog1 = new Catalog();
		catalog1.addDepartment(new Department("Electrical and Computer Engineering", "ECE"));
		catalog1.addDepartment(new Department("Statistics", "STA"));
		catalog1.addCourse(new Course("STA", "Introduction to Bayesisan Theory", 601, 111));
		catalog1.addCourse(new Course("ECE", "Software Engineering", 651, 222));
		catalog1.addCourse(new Course("ECE", "Pattern Recognition", 681, 333));
		BDRoster roster = new BDRoster(catalog1);
		
		try {
			roster.readStudents("./data/students.csv");
			roster.readProfessors("./data/professors.csv");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}


}
