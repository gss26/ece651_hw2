/* RolesTests.java
 * 
 * Same basic unit tests for the roles objects
 */


package tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.util.Vector;
import roles.*;

class RolesTest {

	@Test
	void testRoles() {
		
		//Tests the success of the printing functionality
		Vector<Position> roles = new Vector<Position>();
		roles.add(new Job(20140118, "Duke University", "Grader", "programming assignments"));
		assertEquals(roles.get(0).toString(), "Grader at Duke University where he worked on programming assignments");
		roles.add(new Volunteering(20150303, "ACLU", "Lawyer", "human trafficking"));
		assertEquals(roles.get(1).toString(), "Lawyer at ACLU helping to address human trafficking");
		
	}

	
	@Test
	void testHistory() {
		
		//Tests the printing functionality
		WorkHistory history = new WorkHistory();
		history.addPosition(new Job(20140118, "Duke University", "Grader", "programming assignments"));
		history.addPosition(new Volunteering(20150303, "ACLU", "Lawyer", "human trafficking"));
		history.addPosition(new Volunteering(20101203, "ACLU", "Paralegal", "human trafficking"));
		history.printPositions();
	}

}
